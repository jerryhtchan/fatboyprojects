from random import randrange
from time import gmtime, strftime
import time

def convert2Percentage(dice, numerator1, numerator2, numerator3,  denominator):
    numerator1 = int(numerator1)
    numerator2 = int(numerator2)
    numerator3 = int(numerator3)
    denominator = int(denominator)
    output1 = (1-((denominator-numerator1)/denominator))*100
    output2 = (1-((denominator-numerator2)/denominator))*100
    output3 = (1-((denominator-numerator3)/denominator))*100
    print(dice,":\t",round(output1,3),"%\t",round(output2,3),"%\t",round(output3,3),"%")

print("\nGMT: "+time.strftime("%a, %d %b %Y %I:%M:%S %p %Z", time.gmtime()))
print("Local: "+strftime("%a, %d %b %Y %I:%M:%S %p %Z\n"))

times = input("How Many times do you want to roll? ")

diceFirst = []
diceSecond = []
diceThird = []

x = range(int(times))

for n in x:
    diceFirst.append(randrange(1,7,1))
    diceSecond.append(randrange(1,7,1))
    diceThird.append(randrange(1,7,1))

i = 0

print("\n\tFirst Dice\tSecond Dice\tThird Dice")
convert2Percentage(1, diceFirst.count(1), diceSecond.count(1), diceThird.count(1), len(diceFirst))
convert2Percentage(2, diceFirst.count(2), diceSecond.count(2), diceThird.count(2), len(diceFirst))
convert2Percentage(3, diceFirst.count(3), diceSecond.count(3), diceThird.count(3), len(diceFirst))
convert2Percentage(4, diceFirst.count(4), diceSecond.count(4), diceThird.count(4), len(diceFirst))
convert2Percentage(5, diceFirst.count(5), diceSecond.count(5), diceThird.count(5), len(diceFirst))
convert2Percentage(6, diceFirst.count(6), diceSecond.count(6), diceThird.count(6), len(diceFirst))
