def convert2Percentage(dice, numerator, denominator):
    numerator = int(numerator)
    denominator = int(denominator)
    output = (1-((denominator-numerator)/denominator))*100
    print(dice,":", round(output,3),"%")